/* This program reads a binary file and reads multiple strings of variable length  */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#define STATE_LEN  0
#define STATE_STR  1

int main(int argc, char **argv) {
  if (argc != 2) exit(1);	/* need filename as argument */
  
  FILE *infile = fopen(argv[1], "rb");

  int in;
  uint8_t length = 0;
  uint8_t state = STATE_LEN;
  while ((in = fgetc(infile)) != EOF) {
    if (state == STATE_LEN) {
      length = (uint8_t)in;
      state = STATE_STR;
      printf("\nlength: %d\n", length);
    }
    else {
      printf("%c", in);
      length--;
      if (length == 0) state = STATE_LEN;
    }
  }

  printf("\n");
}
