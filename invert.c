/* This program reads a binary file and checks if input is a valid farbfeld image  */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <arpa/inet.h>

static void invert(uint16_t *rgba) {
  rgba[0] = UINT16_MAX - rgba[0];
  rgba[1] = UINT16_MAX - rgba[1];
  rgba[2] = UINT16_MAX - rgba[2];
}

int main(int argc, char **argv) {
  if (argc != 1) exit(1);	/* need filename as argument */
  
  uint32_t header[4], width, height;

  uint16_t rgba[4];

  fread(header, sizeof(*header), 4, stdin);

  width = ntohl(header[2]);
  height = ntohl(header[3]);

  fwrite(header, sizeof(*header), 4, stdout);

  for (unsigned int i = 0; i < width * height; i++) {
    fread(rgba, sizeof(*rgba), 4, stdin);
    for (i = 0; i < 4; i++) rgba[i] = ntohs(rgba[i]);
    invert(rgba);
    for (i = 0; i < 4; i++) rgba[i] = htons(rgba[i]);
    fwrite(rgba, sizeof(*rgba), 4, stdout);
  }
  return 0;
}
