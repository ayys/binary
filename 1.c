/* This program reads a binary file and prints all bytes as numerical values  */

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
  if (argc != 2) exit(1);	/* need filename as argument */
  
  FILE *infile = fopen(argv[1], "rb");

  int in;
  while ((in = fgetc(infile)) != EOF) printf("%d ", in);
  printf("\n");
}
