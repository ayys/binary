/* This program reads a binary file and reads 2 bytes as x and y coordinates  */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#define STATE_X 0
#define STATE_Y 1

typedef struct point {
  uint8_t x;
  uint8_t y;
} POINT;

int main(int argc, char **argv) {
  if (argc != 2) exit(1);	/* need filename as argument */
  
  FILE *infile = fopen(argv[1], "rb");
  POINT p;
  uint8_t state = STATE_X;
  int in;
  while ((in = fgetc(infile)) != EOF) {
    if (state == STATE_X) {
      p.x = (uint8_t)in;
      state = STATE_Y;
    } else {
      p.y = (uint8_t) in;
      printf("x: %d, y: %d\n", p.x, p.y);
      state = STATE_X;
    }
  }
  printf("\n");
}
