/* This program reads a binary file and checks if input is a valid farbfeld image  */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <arpa/inet.h>

int main(int argc, char **argv) {
  if (argc != 2) exit(1);	/* need filename as argument */
  
  FILE *infile = fopen(argv[1], "rb");

  uint32_t header[4], width, height;

  fread(header, sizeof(*header), 4, infile);

  width = ntohl(header[2]);
  height = ntohl(header[3]);

  printf("%d x %d\n", (int)width, (int)height);

  fclose(infile);

  return 0;
}
