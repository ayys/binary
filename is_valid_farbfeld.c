/* This program reads a binary file and checks if input is a valid farbfeld image  */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char **argv) {
  if (argc != 2) exit(1);	/* need filename as argument */
  
  FILE *infile = fopen(argv[1], "rb");

  char farbfeld[8];
  fread(farbfeld, sizeof(*farbfeld), 8, infile);
  int cmp = strncmp("farbfeld", farbfeld, 8);

  if (cmp == 0) printf("'%s' is a farbfeld image", argv[1]);
  else printf("'%s' is not a farbfeld image", argv[1]);
  printf("\n");
  fclose(infile);


  return 0;
}
