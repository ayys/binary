all:  1 2 3 4 isfarbfeld widthheight invert

1: 1.c
2: 2.c
3: 3.c
4: 4.c
isfarbfeld: is_valid_farbfeld.c
	gcc -o $@ $?
widthheight: get_width_height.c
	gcc -o $@ $?
invert: invert.c
	gcc -o $@ $?

bin: 1bin 2bin 3bin 4bin

clean:
	rm [0-9] isfarbfeld widthheight invert | true

1bin: 1.bin
	printf "%b%b%b" "\x41\x42\x43" > $?

2bin: 2.bin
	printf "%b%b%b%b" "\x41\x42\x43\x44" > $?

3bin: 3.bin
	printf "%b%b%b%b%b" "\x04\x41\x42\x43\x44" > $?

4bin: 4.bin
	printf "%b%b%b%b%b%b%b%b%b" "\x04\x41\x42\x43\x44\x03\x71\x72\x73" > $?


